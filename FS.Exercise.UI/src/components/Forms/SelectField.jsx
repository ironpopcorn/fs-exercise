import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'formik';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';

const SelectField = ({ parent, name, label, children, className }) => (
  <Field
    name={parent + '.' + name}
    render={({ field, form: { touched, errors } }) => (
      <FormControl
        error={(errors[parent] || {})[name] && (touched[parent] || {})[name]}
      >
        <InputLabel htmlFor={parent + '.' + name}>{label}</InputLabel>
        <Select
          native
          {...field}
          input={<Input name={parent + '.' + name} id={parent + '.' + name} />}
          className={className}
          autoWidth
        >
          {children}
        </Select>
        <FormHelperText>
          {(touched[parent] || {})[name] && (errors[parent] || {})[name]}
        </FormHelperText>
      </FormControl>
    )}
  />
);

SelectField.propTypes = {
  parent: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired
};

export default SelectField;
