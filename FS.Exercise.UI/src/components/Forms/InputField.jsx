import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'formik';
import TextField from '@material-ui/core/TextField';

const InputField = ({ parent, name, type, label, placeholder,  className, inputProps }) => (
  <Field
    name={parent + '.' + name}
    render={({ field, form: { touched, errors } }) => (
      <TextField
        className={className}
        {...field}
        label={label}
        type={type}
        error={(errors[parent] || {})[name] && (touched[parent] || {})[name]}
        helperText={
          (touched[parent] || {})[name] && (errors[parent] || {})[name]
        }
        placeholder={placeholder}
        inputProps={inputProps}
      />
    )}
  />
);

InputField.propTypes = {
  parent: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string
};

export default InputField;
