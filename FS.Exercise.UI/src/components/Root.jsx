import React from 'react';
import { Provider } from '../context';
import App from './App';

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token: ''
    }  
  }

  getContext = () => ({
    ...this.state,
    onLoginSuccess: this.handleLoginSuccess
  })

  handleLoginSuccess = token =>
    this.setState(state => ({
      ...state,
      token
    }));

  render() {
    return (
      <Provider value={this.getContext()}>
        <App />
      </Provider>
    );
  }
}
