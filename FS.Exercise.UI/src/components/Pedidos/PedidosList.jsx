import React from 'react';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  right: {
    textAlign: 'right'
  },
  searchBar: {
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)'
  },
  block: {
    display: 'block'
  },
  searchInput: {
    fontSize: theme.typography.fontSize
  }
});

const stableSort = (array, cmp) => {
  const stabilizedThis = array.map((el, index) => [
    {
      ...el,
      razonSocial: el.cliente.razonSocial
    },
    index
  ]);

  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  return stabilizedThis.map(el => el[0]);
}

const getSorting = (order, orderBy) => {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const desc = (a, b, orderBy) => {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

const contains = (a, b) => {
  return a.toString().toLowerCase().indexOf(b.toString().toLowerCase()) >= 0;
}

export default withStyles(styles)(
  class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        order: 'asc',
        orderBy: 'fecha',
        filter: ''
      };
    }

    handleRequestSort = property => event => {
      const orderBy = property;
      let order = 'desc';
  
      if (this.state.orderBy === property && this.state.order === 'desc') {
        order = 'asc';
      }
  
      this.setState({ order, orderBy });
    };

    handleFilterChange = ({ target: { value } }) => {
      this.setState({
        filter: value
      });
    }

    render = () => {
      const { order, orderBy, filter } = this.state;
      const { classes, pedidos, onSelectPedido, onNavClick } = this.props;
      const { loading, data } = pedidos;
      const { pageNumber, pageCount, results, links } = data;

      if (loading) {
        return <Typography>Cargando</Typography>
      }

      if (results === null) {
        return <Typography>Sin datos</Typography>
      }

      let filtered = filter === ''
        ? results
        : results.filter(pedido =>
            contains(pedido.id, filter)
            || contains(pedido.cliente.razonSocial, filter)
            || contains(new Date(pedido.fecha).toLocaleDateString('es-CL'), filter)
          );

      return (
        <div>
          <AppBar className={classes.searchBar} position="static" color="default" elevation={0}>
            <Toolbar>
              <Grid container spacing={16} alignItems="center">
                <Grid item>
                  <SearchIcon className={classes.block} color="inherit" />
                </Grid>
                <Grid item xs>
                  <TextField
                    fullWidth
                    placeholder="Buscar por Nº, Cliente, Fecha"
                    InputProps={{
                      disableUnderline: true,
                      className: classes.searchInput,
                    }}
                    value={filter}
                    onChange={this.handleFilterChange}
                  />
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  <TableSortLabel
                    active={orderBy === 'id'}
                    direction={order}
                    onClick={this.handleRequestSort('id')}
                  >
                    Nº
                  </TableSortLabel>
                </TableCell>
                <TableCell>
                  <TableSortLabel
                    active={orderBy === 'razonSocial'}
                    direction={order}
                    onClick={this.handleRequestSort('razonSocial')}
                  >
                    Cliente
                  </TableSortLabel>
                </TableCell>
                <TableCell>
                  <TableSortLabel
                    active={orderBy === 'fecha'}
                    direction={order}
                    onClick={this.handleRequestSort('fecha')}
                  >
                    Fecha
                  </TableSortLabel>
                </TableCell>
                <TableCell className={classes.right}>
                  <TableSortLabel
                    active={orderBy === 'total'}
                    direction={order}
                    onClick={this.handleRequestSort('total')}
                  >
                    Total
                  </TableSortLabel>
                </TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {stableSort(filtered, getSorting(order, orderBy))
                .map(pedido => (
                <TableRow key={`pedido_${pedido.id}`}>
                  <TableCell>{pedido.id}</TableCell>
                  <TableCell>{pedido.razonSocial}</TableCell>
                  <TableCell>{new Date(pedido.fecha).toLocaleDateString('es-CL')}</TableCell>
                  <TableCell className={classes.right}>{pedido.total}</TableCell>
                  <TableCell>
                    <Button
                      variant='outlined'
                      onClick={() => onSelectPedido(pedido)}
                    >
                      Ver
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <div>
            <Button
              onClick={() => onNavClick(links.first)}
              disabled={links.first === null}
            >
              Primero
            </Button>
            <Button
              onClick={() => onNavClick(links.previous)}
              disabled={links.previous === null}
            >
              Anterior
            </Button>
            <Typography inline>
              {pageNumber} de {pageCount}
            </Typography>
            <Button
              onClick={() => onNavClick(links.next)}
              disabled={links.next === null}
            >
              Siguiente
            </Button>
            <Button
              onClick={() => onNavClick(links.last)}
              disabled={links.last === null}
            >
              Último
            </Button>
            <Typography inline>
              Mostrando {filtered.length} resultados
            </Typography>
          </div>
        </div>
      );
    }
  }
);
