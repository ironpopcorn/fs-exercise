import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { withContext } from '../../context';
import { InputField, SelectField } from '../Forms';

const styles = theme => ({
  row: {
    width: '100%',
    marginBottom: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 150
  },
  selectField: {
    width: 250
  },
  button: {
    marginRight: theme.spacing.unit
  }
});

const validationSchema = Yup.object().shape({
  pedido: Yup.object().shape({
    id: Yup.number().required('Requerido'),
    clienteId: Yup.string().required('Requerido'),
    fecha: Yup.string().required('Requerido'),
    glosa: Yup.string()
  })
});

export default withContext(withStyles(styles)(
  ({
    classes,
    pedido,
    clientes,
    productos,
    onEditSubmit,
    onCancelEditModeClick
  }) => {
    let initialPedido = {
      pedido: {
        ...pedido,
        fecha: pedido.fecha.substring(0, 10)
      }
    };

    return (
      <Formik
        enableReinitialize
        initialValues={initialPedido}
        validationSchema={validationSchema}
        onSubmit={onEditSubmit}
      >
        {({ isSubmitting, values, dirty }) => {
          return (
            <Form autoComplete='off'>
              <div className={classes.row}>
                <SelectField
                  className={classes.selectField}
                  parent='pedido'
                  name='clienteId'
                  label='Cliente'
                >
                  <option value='' />
                  {!clientes.loading
                    ? clientes.data.map(cliente => (
                        <option
                          key={`cliente_${cliente.id}`}
                          value={cliente.id}
                        >
                          {cliente.razonSocial}
                        </option>
                      ))
                    : null
                  }
                </SelectField>
                <InputField
                  className={classes.textField}
                  parent='pedido'
                  name='fecha'
                  type='date'
                  label='Fecha'
                />
                <InputField
                  className={classes.textField}
                  parent='pedido'
                  name='glosa'
                  type='text'
                  defaultValue={pedido.glosa}
                  label='Glosa'
                />
              </div>
              <div className={classes.row}>
              {pedido.detalles.map((detalle, i) => (
                <div key={`detalle_${detalle.id}`}>
                    <SelectField
                      className={classes.selectField}
                      parent={`pedido.detalles[${i}]`}
                      name='productoId'
                      label='Producto'
                    >
                      <option value='' />
                      {!productos.loading
                        ? productos.data.map(producto => (
                            <option
                              key={`producto_${producto.id}`}
                              value={producto.id}
                            >
                              {producto.descripcion}
                            </option>
                          ))
                        : null
                      }
                    </SelectField>
                    <InputField
                      className={classes.textField}
                      parent={`pedido.detalles[${i}]`}
                      name='precio'
                      type='number'
                      defaultValue={detalle.precio}
                      label='Precio'
                      inputProps={{ min: 1 }}
                    />
                    <InputField
                      className={classes.textField}
                      parent={`pedido.detalles[${i}]`}
                      name='cantidad'
                      type='number'
                      defaultValue={detalle.cantidad}
                      label='Cantidad'
                      inputProps={{ min: 1 }}
                    />
                    <TextField
                      disabled
                      className={classes.textField}
                      label='Total'
                      type='text'
                      value={values.pedido.detalles[i].cantidad * values.pedido.detalles[i].precio}
                    />
                </div>
              ))}
              </div>
              <div>
                <Button
                  className={classes.button}
                  disabled={isSubmitting}
                  variant='outlined'
                  onClick={onCancelEditModeClick}
                >
                  Cancelar
                </Button>
                <Button
                  className={classes.button}
                  disabled={isSubmitting || !dirty}
                  color='primary'
                  variant='contained'
                  type='submit'
                >
                  {isSubmitting ? 'Guardando' : 'Guardar'}
                </Button>
              </div>
            </Form>
          )}
        }
      </Formik>
    );
  }
));
