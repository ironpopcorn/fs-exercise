import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import PedidoView from './PedidoView';
import PedidoEdit from './PedidoEdit';

const styles = theme => ({
  row: {
    width: '100%',
    marginBottom: theme.spacing.unit * 2
  },
  fullwidht: {
    width: '100%'
  }
});

export default withStyles(styles)(
  ({ 
    classes,
    pedido,
    editMode,
    onCancelSelectClick
  }) => (
    <Grid
      container
      direction='column'
      justify='center'
      alignItems='center'
    >
      <Grid
        item
        className={classes.fullwidht}
      >
        <Grid
          className={classes.row}
          container
          direction='row'
          justify='space-between'
          alignItems='stretch'
        >
          <Grid item>
            <Typography variant='h5'>
              Pedido Nº {pedido.id}
            </Typography>
          </Grid>
          <Grid item>
            <Button
              variant='outlined'
              onClick={onCancelSelectClick}
            >
              Volver
            </Button>
          </Grid>
        </Grid>
      </Grid>
      <Grid
        item
        className={classes.fullwidht}
      >
      {
        editMode
        ? <PedidoEdit pedido={pedido} />
        : <PedidoView pedido={pedido} />
      }
      </Grid>            
    </Grid>
  )
);