import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import { withContext } from '../../context';

const styles = theme => ({
  row: {
    width: '100%',
    marginBottom: theme.spacing.unit * 2
  },
  button: {
    marginRight: theme.spacing.unit
  },
  right: {
    textAlign: 'right'
  }
});

export default withContext(withStyles(styles)(
  ({
    classes,
    pedido,
    onEnterEditModeClick
  }) => (
    <Grid container>
      <Grid
        className={classes.row}
        item
      >
        <Typography>
          <strong>Rut: </strong>{pedido.cliente.rut}
        </Typography>
        <Typography>
          <strong>Razón Social: </strong>{pedido.cliente.razonSocial}
        </Typography>
        <Typography>
          <strong>Dirección: </strong>{pedido.cliente.direccion}
        </Typography>
        <Typography>
          <strong>Teléfono: </strong>{pedido.cliente.telefono}
        </Typography>
        <Typography>
          <strong>Fecha: </strong>{new Date(pedido.fecha).toLocaleDateString('es-CL')}
        </Typography>
        <Typography>
          <strong>Glosa: </strong>{pedido.glosa}
        </Typography>
      </Grid>
      <Grid
        className={classes.row}
        item
      >
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Producto</TableCell>
              <TableCell className={classes.right}>Precio</TableCell>
              <TableCell className={classes.right}>Cantidad</TableCell>
              <TableCell className={classes.right}>Total</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {pedido.detalles.map(detalle => (
              <TableRow key={`detalle_${detalle.id}`}>
                <TableCell>{detalle.producto.descripcion}</TableCell>
                <TableCell className={classes.right}>{detalle.precio}</TableCell>
                <TableCell className={classes.right}>{detalle.cantidad}</TableCell>
                <TableCell className={classes.right}>{detalle.total}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Grid>
      <Grid
        className={classes.row}
        item
      >
        <Typography
          className={classes.right}
          variant='h5'
        >
          Total: {pedido.total}
        </Typography>
      </Grid>
      <Grid item>
        <Button
          className={classes.button}
          variant='outlined'
          onClick={onEnterEditModeClick}
        >
          Editar
        </Button>
      </Grid>
    </Grid>
  )
));
