import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { InputField } from './Forms';
import { withContext } from '../context';
import { API_URL, ORIGIN } from '../config';
import ErrorMessage from './ErrorMessage';

const styles = theme => ({
  container: {
    margin: theme.spacing.unit,
  },
  header: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    background: theme.palette.primary.main,
    color: theme.palette.primary.contrastText
  },
  body: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  footer: {
    paddingTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    display: 'block'
  },
  button: {
    margin: theme.spacing.unit
  }
});

const initialValues = {
  login: {
    username: '',
    password: ''
  }
};

const validationSchema = Yup.object().shape({
  login: Yup.object().shape({
    username: Yup.string().required('Requerido'),
    password: Yup.string().required('Requerido')
  })
});

export default withContext(withStyles(styles)(
  class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        error: null
      };
    }

    handleSubmit = ({ login }, { setSubmitting, setErrors, setStatus, resetForm }) => {
      this.setState({
        error: null
      });

      fetch(API_URL + '/login/autenticar', {
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify({
          ...login,
        }),
        headers: new Headers({
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Origin': ORIGIN
        })
      })
      .then(response => {
        if(response.ok) {
          return response.json();
        } else {
          throw response;
        }
      })
      .then(token => {
        if(token !== '') {
          resetForm(initialValues);
          setStatus({ success: true });
          this.props.onLoginSuccess(token);
        } else {
          throw token;
        }
      })
      .catch(error => {
        console.log(error);
        if(error.status === 401) {
          this.setState({
            error: 'Credenciales inválidas'
          });
        } else {
          this.setState({
            error: 'Ocurrió un error al validar las credenciales'
          });
        }
        setStatus({ success: false });
        setSubmitting(false);
        setErrors({ submit: error.message });
      });
    }

    render = () => {
      const { classes } = this.props;
      const { error } = this.state;

      return (
        <Grid
          container
          direction='row'
          justify='center'
          alignItems='center'
        >
          <Grid
            item
            className={classes.container}
          >
            <Paper>
              <Typography
                variant='h4'
                className={classes.header}
              >
                Iniciar Sesión
              </Typography>
              <ErrorMessage message={error} />
              <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={this.handleSubmit}
              >
              {({ isSubmitting }) => {
                return (
                  <Form autoComplete='off'>
                    <div className={classes.body}>
                      <InputField
                        className={classes.textField}
                        parent='login'
                        name='username'
                        type='text'
                        label='Usuario'
                      />
                      <InputField
                        className={classes.textField}
                        parent='login'
                        name='password'
                        type='password'
                        label='Contraseña'
                      />
                    </div>
                    <div className={classes.footer}>
                      <Button
                        className={classes.button}
                        disabled={isSubmitting}
                        color='primary'
                        variant='contained'
                        type='submit'
                      >
                        {isSubmitting ? 'Autenticando' : 'Entrar'}
                      </Button>
                    </div>
                  </Form>
              )}}
              </Formik>
            </Paper>
          </Grid>
        </Grid>
      );
    }
  }
));
