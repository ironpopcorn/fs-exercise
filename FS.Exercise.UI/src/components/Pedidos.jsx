import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { Provider, withContext } from '../context';
import { API_URL, ORIGIN } from '../config';
import PedidosList from './Pedidos/PedidosList';
import PedidoSelected from './Pedidos/PedidoSelected';

const styles = theme => ({
  container: {
    margin: theme.spacing.unit,
    width: 805
  },
  header: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    background: theme.palette.primary.main,
    color: theme.palette.primary.contrastText
  },
  body: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  row: {
    width: '100%',
    marginBottom: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 150
  },
  selectField: {
    width: 250
  },
  button: {
    marginRight: theme.spacing.unit
  },
  fullwidht: {
    width: '100%'
  },
  right: {
    textAlign: 'right'
  }
});

const pedidos = {
  loading: false,
  error: null,
  filter: '',
  data: {
    pageNumber: 1,
    pageSize: 10,
    pageCount: 10,
    results: null,
    links: {
      first: null,
      previous: null,
      current: null,
      next: null,
      last: null
    }
  },
  selected: null,
  editMode: false
};

const clientes = {
  loading: false,
  data: null,
  selected: null
};

const productos = {
  loading: false,
  data: null,
  selected: null
};

export default withContext(withStyles(styles)(
  class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        pedidos,
        clientes,
        productos
      };
    }

    componentDidMount = () =>
      this.loadPedidos('/pedidos');

    loadPedidos = url => {
      const { token } = this.props;

      this.setState({
        pedidos: {
          ...pedidos,
          loading: true
        }
      });

      fetch(API_URL + url, {
        method: 'GET',
        mode: 'cors',
        headers: new Headers({
          'Authorization': `Bearer ${token}`,
          'Accept': 'application/json',
          'Origin': ORIGIN
        })
      })
      .then(response => {
        if(response.ok) {
          return response.json();
        } else {
          throw response;
        }
      })
      .then(data => {
        this.setState({
          pedidos: {
            ...pedidos,
            data
          }
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          pedidos: {
            ...pedidos,
            error
          }
        });
      });
    }

    loadClientes = () => {
      const { token } = this.props;

      this.setState({
        clientes: {
          ...clientes,
          loading: true
        }
      });

      fetch(API_URL + '/clientes', {
        method: 'GET',
        mode: 'cors',
        headers: new Headers({
          'Authorization': `Bearer ${token}`,
          'Accept': 'application/json',
          'Origin': ORIGIN
        })
      })
      .then(response => {
        if(response.ok) {
          return response.json();
        } else {
          throw response;
        }
      })
      .then(data => {
        this.setState({
          clientes: {
            ...clientes,
            data
          }
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          clientes: {
            ...clientes,
            error
          }
        });
      });
    }

    loadProductos = () => {
      const { token } = this.props;

      this.setState({
        productos: {
          ...productos,
          loading: true
        }
      });

      fetch(API_URL + '/productos', {
        method: 'GET',
        mode: 'cors',
        headers: new Headers({
          'Authorization': `Bearer ${token}`,
          'Accept': 'application/json',
          'Origin': ORIGIN
        })
      })
      .then(response => {
        if(response.ok) {
          return response.json();
        } else {
          throw response;
        }
      })
      .then(data => {
        this.setState({
          productos: {
            ...productos,
            data
          }
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          productos: {
            ...productos,
            error
          }
        });
      });
    }

    handleSelectClick = selected =>
      this.setState(state => ({
        pedidos: {
          ...state.pedidos,
          selected,
          editMode: false
        }
      }));

    handleCancelSelectClick = () =>
      this.setState(state => ({
        pedidos: {
          ...state.pedidos,
          selected: null,
          editMode: false
        }
      }));

    handleNavClick = url =>
      this.loadPedidos(url);

    handleEnterEditModeClick = () => {
      this.loadClientes();
      this.loadProductos();

      this.setState(state => ({
        pedidos: {
          ...state.pedidos,
          editMode: true
        }
      }));
    }

    handleCancelEditModeClick = () =>
      this.setState(state => ({
        pedidos: {
          ...state.pedidos,
          editMode: false
        }
      }));

    handleEditSubmit = ({ pedido }, { setSubmitting, setErrors }) => {
      const { token } = this.props;
      
      fetch(API_URL + '/pedidos/?id=' + pedido.id, {
        method: 'PUT',
        mode: 'cors',
        body: JSON.stringify({
          ...pedido,
        }),
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }),
      })
      .then(response => {
        if(response.ok) {
          this.loadPedidos('/pedidos');
        } else {
          throw response;
        }
      })
      .catch(error => {
        console.log(error);
        setSubmitting(false);
        setErrors(true);
      });
    }

    getPedidoContext = () => ({
      ...this.state,
      onEnterEditModeClick: this.handleEnterEditModeClick,
      onCancelEditModeClick: this.handleCancelEditModeClick,
      onEditSubmit: this.handleEditSubmit
    });

    render = () => {
      const { classes } = this.props;
      const { pedidos } = this.state;
      const { selected, editMode } = pedidos;
      
      return (
        <Grid
          container
          direction='row'
          justify='center'
          alignItems='center'
        >
          <Grid
            item
            className={classes.container}
          >
            <Paper>
              <Typography
                variant='h4'
                className={classes.header}
              >
                Pedidos
              </Typography>
              <div className={classes.body}>
              {selected === null
                ? <PedidosList
                    pedidos={pedidos}
                    onSelectPedido={this.handleSelectClick}
                    onNavClick={this.handleNavClick}
                  />
                : <Provider value={this.getPedidoContext()}>
                    <PedidoSelected
                      pedido={selected}
                      editMode={editMode}
                      onCancelSelectClick={this.handleCancelSelectClick}
                    />
                  </Provider>
              }
              </div>
            </Paper>
          </Grid>
        </Grid>
      );
    }
  }
));
