import React from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  container: {
    width: '100%',
    background: theme.palette.error.light,
    padding: theme.spacing.unit
  },
  text: {
    color: theme.palette.error.contrastText
  }
});

export default withStyles(styles)(
  ({
    classes,
    message
  }) => (
    message &&
    <div className={classes.container}>
    <Typography
    className={classes.text}
    >
    {message}
    </Typography>
    </div>
  )
);
