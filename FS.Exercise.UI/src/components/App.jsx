import React from 'react';
import { MuiThemeProvider, createMuiTheme, withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { withContext } from '../context';
import Login from './Login';
import Pedidos from './Pedidos';

let theme = createMuiTheme({
  typography: {
    useNextVariants: true
  }
});

const appStyles = {
  root: {
    display: 'flex',
    minHeight: '100vh',
  },
  appContent: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
  },
  mainContent: {
    flex: 1,
    background: '#eaeff1',
  },
};

export default withContext(withStyles(appStyles)(
  class extends React.Component {
    render() {
      const { classes, token } = this.props;

      return (
        <MuiThemeProvider theme={theme}>
          <div className={classes.root}>
            <CssBaseline />
            <div className={classes.appContent}>
              <main className={classes.mainContent}>
                {
                  token === ''
                  ? <Login />
                  : <Pedidos />
                }                
              </main>
            </div>
          </div>
        </MuiThemeProvider>
      );
    }
  }
));
