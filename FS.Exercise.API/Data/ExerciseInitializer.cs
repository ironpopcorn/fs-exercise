﻿using FS.Exercise.API.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace FS.Exercise.API.Data
{
    public class ExerciseInitializer : DropCreateDatabaseIfModelChanges<ExerciseContext>
    {
        protected override void Seed(ExerciseContext context)
        {
            // Clientes
            var clientes = new List<Cliente>
            {
                new Cliente("76089369-4", "Almacenes Fany", "Las Acacias 43, Santiago", "224348956") { Id = 1 },
                new Cliente("76589475-K", "Panaderia San Carlos", "Av Matta 789, Santiago", "+56223057912") { Id = 2 },
                new Cliente("7851469-2", "Gabriela Rojas", "Los Leones 630, Providencia", "+56995609984") { Id = 3 },
                new Cliente("77070963-6", "Industria Picure", "Av Grecia 550, Ñuñoa", "228054612") { Id = 4 }
            };

            // Productos
            var productos = new List<Producto>()
            {
                new Producto("Arroz Pregraneado Tucapel 1Kg (20 unidades)") { Id = 1 },
                new Producto("Pasta Larga Luccetti 400g (20 unidades)") { Id = 2 },
                new Producto("Atún Robinso Crusoe 300g (10 unidades)") { Id = 3 },
                new Producto("Harina Montalban Sin Polvo 1Kg (50 unidades)") { Id = 4 },
                new Producto("Azúcar Iansa 1Kg (10 unidades)") { Id = 5 }
            };

            // Pedidos
            var random = new Random();
            var pedidos = new List<Pedido>();
            var pedidosDetalles = new List<PedidoDetalle>();

            for (int i = 1; i <= 50; i++)
            {
                pedidos.Add(new Pedido(DateTime.Now, random.Next(1, 5)) { Id = i });

                // Pedidos Detalles
                var idDetalleInicial = pedidosDetalles.Count + 1;
                var idDetalleFinal = pedidosDetalles.Count + random.Next(1, 4);

                for (int j = idDetalleInicial; j <= idDetalleFinal; j++)
                {
                    pedidosDetalles.Add(new PedidoDetalle(random.Next(1, 5), random.Next(1, 7) * 1500, random.Next(1, 6), i) { Id = j });
                }
            }

            context.Clientes.AddRange(clientes);
            context.Productos.AddRange(productos);
            context.Pedidos.AddRange(pedidos);
            context.PedidosDetalles.AddRange(pedidosDetalles);
            context.SaveChanges();
        }
    }
}