﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FS.Exercise.API.Models
{
    [Table("Clientes")]
    public class Cliente
    {
        public int Id { get; set; }

        [Required]
        [RegularExpression("^[0-9]{6,8}[-|‐]{1}[0-9kK]{1}$",
            ErrorMessage = "El Rut ingresado no tiene el formato correcto")]
        public string Rut { get; set; }

        [Required]
        [DisplayName("Razón Social")]
        public string RazonSocial { get; set; }

        [Required]
        [DisplayName("Dirección")]
        public string Direccion { get; set; }

        [Required]
        [DisplayName("Teléfono")]
        [RegularExpression(@"^(\+56)?[9|\d{2}]\d{7,8}$",
            ErrorMessage = "El Teléfono ingresado no tiene el formato correcto")]
        public string Telefono { get; set; }

        public Cliente(string rut, string razonSocial, string direccion, string telefono)
        {
            Rut = rut;
            RazonSocial = razonSocial;
            Direccion = direccion;
            Telefono = telefono;
        }

        public Cliente() { }
    }
}