﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace FS.Exercise.API.Models
{
    [Table("Pedidos")]
    public class Pedido
    {
        public int Id { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",
            ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        public string Glosa { get; set; }

        [Required]
        [ForeignKey("Cliente")]
        public int ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }

        public ICollection<PedidoDetalle> Detalles { get; set; }

        public double Total => Detalles != null ? Detalles.Sum(d => d.Total) : 0;

        public Pedido(DateTime fecha, int clienteId, string glosa)
        {
            Fecha = fecha;
            ClienteId = clienteId;
            Glosa = glosa;
        }

        public Pedido(DateTime fecha, int clienteId)
            : this(fecha, clienteId, string.Empty) { }

        public Pedido() { }
    }
}