﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FS.Exercise.API.Models
{
    [Table("Productos")]
    public class Producto
    {
        public int Id { get; set; }

        [Required]
        [DisplayName("Descripción")]
        public string Descripcion { get; set; }

        public Producto(string descripcion)
        {
            Descripcion = descripcion;
        }

        public Producto() { }
    }
}