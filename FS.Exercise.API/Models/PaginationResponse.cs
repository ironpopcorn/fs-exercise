﻿using System.Collections.Generic;

namespace FS.Exercise.API.Models
{
    public class PaginationResponse<T>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int PageCount { get; set; }
        public IEnumerable<T> Results { get; set; }
        public Links Links { get; set; }
    }
}