﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FS.Exercise.API.Models
{
    [Table("PedidosDetalle")]
    public class PedidoDetalle
    {
        public int Id { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Cantidad inválida")]
        public int Cantidad { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Precio inválido")]
        public int Precio { get; set; }

        [Required]
        [ForeignKey("Producto")]
        public int ProductoId { get; set; }
        public virtual Producto Producto { get; set; }

        [Required]
        [ForeignKey("Pedido")]
        public int PedidoId { get; set; }
        public virtual Pedido Pedido { get; set; }

        public double Total => Cantidad * Precio;

        public PedidoDetalle(int cantidad, int precio, int productoId, int pedidoId)
        {
            Cantidad = cantidad;
            Precio = precio;
            ProductoId = productoId;
            PedidoId = pedidoId;
        }

        public PedidoDetalle() { }
    }
}