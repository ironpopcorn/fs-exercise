﻿namespace FS.Exercise.API.Models
{
    public class Links
    {
        private readonly string url;
        private string first;
        private string previous;
        private string next;
        private string last;

        public string First
        {
            get => GetUrl(first);
            set => first = value;
        }
        public string Previous
        {
            get => GetUrl(previous);
            set => previous = value;
        }
        public string Next
        {
            get => GetUrl(next);
            set => next = value;
        }
        public string Last
        {
            get => GetUrl(last);
            set => last = value;
        }

        public Links(string url)
        {
            this.url = url;
        }

        private string GetUrl(string pageNumber)
        {
            return pageNumber != "" ? url + pageNumber : null;
        }
    }
}