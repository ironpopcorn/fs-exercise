﻿namespace FS.Exercise.API.Models
{
    public class PaginationRequest
    {
        const int MIN_PAGE_NUMBER = 1;
        const int MAX_PAGE_SIZE = 10;

        private int pageNumber = 1;
        private int pageSize = 10;

        public int PageNumber
        {
            get => pageNumber;
            set
            {
                pageNumber = value < MIN_PAGE_NUMBER ? MIN_PAGE_NUMBER : value;
            }
        }

        public int PageSize
        {
            get => pageSize;
            set
            {
                pageSize = value > MAX_PAGE_SIZE ? MAX_PAGE_SIZE : value;
            }
        }

    }
}