﻿using FS.Exercise.API.Models;
using FS.Exercise.API.Security;
using System.Web.Http;

namespace FS.Exercise.API.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        [HttpPost]
        public IHttpActionResult Autenticar([FromBody] LoginRequest login)
        {
            if (login == null)
                return BadRequest();

            bool isCredentialValid = login.Password == "123456"; // TODO: Realizar el login correctamente

            if (isCredentialValid)
            {
                var token = TokenGenerator.GenerateTokenJwt(login.Username);
                return Ok(token);
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}
