﻿using FS.Exercise.API.Data;
using System.Linq;
using System.Web.Http;

namespace FS.Exercise.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/productos")]
    public class ProductosController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get()
        {
            using (var context = new ExerciseContext())
            {
                return Ok(context.Productos.OrderBy(p => p.Descripcion).ToList());
            }
        }
    }
}