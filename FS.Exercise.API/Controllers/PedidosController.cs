﻿using FS.Exercise.API.Data;
using FS.Exercise.API.Models;
using System;
using System.Linq;
using System.Web.Http;

namespace FS.Exercise.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/pedidos")]
    public class PedidosController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get([FromUri] PaginationRequest pagination)
        {
            using (var context = new ExerciseContext())
            {
                var totalCount = context.Pedidos.Count();

                var pedidos = context.Pedidos
                                .Include("Cliente")
                                .Include("Detalles")
                                .Include("Detalles.Producto")
                                .OrderBy(p => p.Fecha)
                                .Skip((pagination.PageNumber - 1) * pagination.PageSize)
                                .Take(pagination.PageSize)
                                .ToList();

                var pageCount = Convert.ToInt32(Math.Ceiling((decimal)totalCount / pagination.PageSize));
                var links = new Links("/pedidos?pageNumber=")
                {
                    First = pagination.PageNumber <= 1 ? "" : "1",
                    Previous = pagination.PageNumber <= 1 ? "" : (pagination.PageNumber - 1).ToString(),
                    Next = pagination.PageNumber >= pageCount ? "" : (pagination.PageNumber + 1).ToString(),
                    Last = pagination.PageNumber >= pageCount ? "" : pageCount.ToString()
                };

                var response = new PaginationResponse<Pedido>()
                {
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    PageCount = pageCount,
                    Results = pedidos,
                    Links = links
                };

                return Ok(response);
            }
        }

        [HttpPut]
        public IHttpActionResult Put([FromUri] int id, [FromBody] Pedido value)
        {
            if (id <= 0)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest("Datos inválidos");

            using (var context = new ExerciseContext())
            {
                var pedido = context.Pedidos
                                        .Include("Cliente")
                                        .Include("Detalles")
                                        .FirstOrDefault(p => p.Id == id);

                if (pedido == null)
                    return NotFound();

                pedido.Fecha = value.Fecha;
                pedido.Glosa = value.Glosa;
                pedido.ClienteId = value.ClienteId;

                var i = 0;
                foreach (var detalle in pedido.Detalles)
                {
                    var newDetalle = value.Detalles.ElementAt(i);

                    detalle.ProductoId = newDetalle.ProductoId;
                    detalle.Precio = newDetalle.Precio;
                    detalle.Cantidad = newDetalle.Cantidad;

                    i++;
                }

                if (context.SaveChanges() > 0)
                    return Ok();

                return InternalServerError();
            }
        }
    }
}