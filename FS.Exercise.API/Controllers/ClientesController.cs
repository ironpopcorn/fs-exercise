﻿using FS.Exercise.API.Data;
using System.Linq;
using System.Web.Http;

namespace FS.Exercise.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/clientes")]
    public class ClientesController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get()
        {
            using (var context = new ExerciseContext())
            {
                return Ok(context.Clientes.OrderBy(c => c.RazonSocial).ToList());
            }
        }
    }
}