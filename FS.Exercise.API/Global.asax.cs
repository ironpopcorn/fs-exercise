﻿using Newtonsoft.Json.Serialization;
using System.Web;
using System.Web.Http;

namespace FS.Exercise.API
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            GlobalConfiguration.Configuration
                                    .Formatters.XmlFormatter.SupportedMediaTypes.Clear();

            GlobalConfiguration.Configuration
                                    .Formatters
                                    .JsonFormatter
                                    .SerializerSettings
                                    .ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
